# Alumni Network Case Project Web API

This is the Web API for the Alumni Network case project for Noroff Accelerate winter 2022. The documentation of the web API can be found here:
https://alumniwebapi.azurewebsites.net/swagger

## Description

This project uses a code first approach using entity framework to create a SQL Server database with these tables:
* User table
* Post table
* Group table
* Topic table
* M2M linking table between User and Topic
* M2M linking table between User and Group

The web API exposes several endpoints to get, post or update information from the database.


## Getting Started

### Dependencies

* .Net Framework
* Visual Studio 2019/22 OR Visual Studio Code
* SQL Server

### Before Executing
* Remember to set your SQL server data source in the data source field in the connection string in the appsetting.json file

### Executing program
* clone repository / download
* Open solution in Visual Studio
* Build and run with IIS Express
* Alternatively a docker image of the web API can be found here: registry.gitlab.com/alumnicaseprojectgroup/alumnibackend:latest
* The web API is also reachable here: https://alumniwebapi.azurewebsites.net/

## Author

Jessica Lindqvist [@Jessica.Lindqvist]
Lukas Mårtensson [@Hela042]
Christian Andersz [@christianandersz]
