﻿using AlumniWebAPI.Models.Domain;
using AlumniWebAPI.Models.DTO.Topic;
using AutoMapper;

namespace AlumniWebAPI.Profiles
{
    public class TopicProfile : Profile
    {
        public TopicProfile()
        {
            CreateMap<Topic, TopicReadDTO>()
                .ReverseMap();
            CreateMap<Topic, TopicCreateDTO>()
                .ReverseMap();
        }
    }
}
