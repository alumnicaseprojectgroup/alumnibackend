﻿using AlumniWebAPI.Models.Domain;
using AlumniWebAPI.Models.DTO.User;
using AutoMapper;
using System.Linq;

namespace AlumniWebAPI.Profiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<User, UserReadDTO>()
                .ForMember(userDTO => userDTO.Groups, options => options
                .MapFrom(u => u.Groups.Select(u => u.GroupId).ToList()))
                .ForMember(userDto => userDto.Topics, options => options
                .MapFrom(u => u.SubscribedTopics.Select(t => t.TopicId).ToList()))
                .ReverseMap();
            CreateMap<User, UserUpdateDTO>()
                .ReverseMap();
            CreateMap<User, UserCreateDTO>()
                .ReverseMap();
        }
    }
}
