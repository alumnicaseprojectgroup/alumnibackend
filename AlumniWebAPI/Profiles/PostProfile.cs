﻿using AlumniWebAPI.Models.Domain;
using AlumniWebAPI.Models.DTO.Post;
using AutoMapper;

namespace AlumniWebAPI.Profiles
{
    public class PostProfile : Profile
    {
        public PostProfile()
        {
            CreateMap<Post, PostReadDTO>()
                .ForMember(dto => dto.posterName, options => options
                .MapFrom(p => p.UserSender.Username))
                .ForMember(dto => dto.TargetName, options => options
                .MapFrom(p => p.Target.Username))
                .ForMember(dto => dto.GroupName, options => options
                .MapFrom(p => p.TargetGroup.Name))
                .ForMember(dto => dto.TopicName, options => options
                .MapFrom(p => p.Topic.Name))
                .ReverseMap();
            CreateMap<Post, PostCreateDTO>()
                .ReverseMap();
            CreateMap<Post, PostUpdateDTO>()
                .ReverseMap();
            CreateMap<PostReadDTO, PostUserDTO>()
                .ReverseMap();
        }
    }
}
