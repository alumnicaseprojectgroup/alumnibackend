﻿using AlumniWebAPI.Models.Domain;
using AlumniWebAPI.Models.DTO.Group;
using AutoMapper;

namespace AlumniWebAPI.Profiles
{
    public class GroupProfile : Profile
    {
        public GroupProfile()
        {
            CreateMap<Group, GroupReadDTO>()
                .ReverseMap();
            CreateMap<Group, GroupCreateDTO>()
                .ReverseMap();
        }
    }
}
