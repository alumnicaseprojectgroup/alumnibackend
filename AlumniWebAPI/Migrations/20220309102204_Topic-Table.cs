﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AlumniWebAPI.Migrations
{
    public partial class TopicTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "TopicId",
                table: "Posts",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Topics",
                columns: table => new
                {
                    TopicId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Topics", x => x.TopicId);
                });

            migrationBuilder.CreateTable(
                name: "TopicUser",
                columns: table => new
                {
                    SubscribedTopicsTopicId = table.Column<int>(type: "int", nullable: false),
                    SubscribersUserId = table.Column<string>(type: "nvarchar(50)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TopicUser", x => new { x.SubscribedTopicsTopicId, x.SubscribersUserId });
                    table.ForeignKey(
                        name: "FK_TopicUser_Topics_SubscribedTopicsTopicId",
                        column: x => x.SubscribedTopicsTopicId,
                        principalTable: "Topics",
                        principalColumn: "TopicId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TopicUser_Users_SubscribersUserId",
                        column: x => x.SubscribersUserId,
                        principalTable: "Users",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "PostId",
                keyValue: 2,
                column: "TimeStamp",
                value: new DateTime(2022, 3, 9, 11, 22, 4, 130, DateTimeKind.Local).AddTicks(9178));

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "PostId",
                keyValue: 3,
                column: "TimeStamp",
                value: new DateTime(2022, 3, 9, 11, 22, 4, 130, DateTimeKind.Local).AddTicks(9468));

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "PostId",
                keyValue: 4,
                column: "TimeStamp",
                value: new DateTime(2022, 3, 9, 11, 22, 4, 130, DateTimeKind.Local).AddTicks(9709));

            migrationBuilder.InsertData(
                table: "Topics",
                columns: new[] { "TopicId", "Description", "Name" },
                values: new object[,]
                {
                    { 1, "Fun animal facts.", "Animals" },
                    { 2, "Anything related to work.", "Work" }
                });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "PostId",
                keyValue: 1,
                columns: new[] { "TimeStamp", "TopicId" },
                values: new object[] { new DateTime(2022, 3, 9, 11, 22, 4, 128, DateTimeKind.Local).AddTicks(5535), 2 });

            migrationBuilder.CreateIndex(
                name: "IX_Posts_TopicId",
                table: "Posts",
                column: "TopicId");

            migrationBuilder.CreateIndex(
                name: "IX_TopicUser_SubscribersUserId",
                table: "TopicUser",
                column: "SubscribersUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Posts_Topics_TopicId",
                table: "Posts",
                column: "TopicId",
                principalTable: "Topics",
                principalColumn: "TopicId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Posts_Topics_TopicId",
                table: "Posts");

            migrationBuilder.DropTable(
                name: "TopicUser");

            migrationBuilder.DropTable(
                name: "Topics");

            migrationBuilder.DropIndex(
                name: "IX_Posts_TopicId",
                table: "Posts");

            migrationBuilder.DropColumn(
                name: "TopicId",
                table: "Posts");

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "PostId",
                keyValue: 1,
                column: "TimeStamp",
                value: new DateTime(2022, 3, 9, 11, 7, 6, 374, DateTimeKind.Local).AddTicks(3904));

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "PostId",
                keyValue: 2,
                column: "TimeStamp",
                value: new DateTime(2022, 3, 9, 11, 7, 6, 376, DateTimeKind.Local).AddTicks(5096));

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "PostId",
                keyValue: 3,
                column: "TimeStamp",
                value: new DateTime(2022, 3, 9, 11, 7, 6, 376, DateTimeKind.Local).AddTicks(5382));

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "PostId",
                keyValue: 4,
                column: "TimeStamp",
                value: new DateTime(2022, 3, 9, 11, 7, 6, 376, DateTimeKind.Local).AddTicks(5605));
        }
    }
}
