﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AlumniWebAPI.Migrations
{
    public partial class newdbCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    UserId = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Username = table.Column<string>(type: "nvarchar(70)", maxLength: 70, nullable: false),
                    FirstName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    LastName = table.Column<string>(type: "nvarchar(70)", maxLength: 70, nullable: true),
                    Picture = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: true),
                    emailAdress = table.Column<string>(type: "nvarchar(70)", maxLength: 70, nullable: true),
                    Status = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    Bio = table.Column<string>(type: "nvarchar(400)", maxLength: 400, nullable: true),
                    FunFact = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.UserId);
                });

            migrationBuilder.CreateTable(
                name: "Posts",
                columns: table => new
                {
                    PostId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TimeStamp = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ParentPostId = table.Column<int>(type: "int", nullable: true),
                    SenderId = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    TargetId = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    PostBody = table.Column<string>(type: "nvarchar(400)", maxLength: 400, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Posts", x => x.PostId);
                    table.ForeignKey(
                        name: "FK_Posts_Posts_ParentPostId",
                        column: x => x.ParentPostId,
                        principalTable: "Posts",
                        principalColumn: "PostId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Posts_Users_SenderId",
                        column: x => x.SenderId,
                        principalTable: "Users",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Posts_Users_TargetId",
                        column: x => x.TargetId,
                        principalTable: "Users",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "UserId", "Bio", "FirstName", "FunFact", "LastName", "Picture", "Status", "Username", "emailAdress" },
                values: new object[] { "622867945b5a0b0070055011", "My name is John.", "John", "Can flap his ears.", "Johnson", "https://randomuser.me/api/portraits/men/61.jpg", "unemployed :(.", "JohnJohnson", "JohnJ@fake.com" });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "UserId", "Bio", "FirstName", "FunFact", "LastName", "Picture", "Status", "Username", "emailAdress" },
                values: new object[] { "622868348904e60069c28aa9", "My name is Jane.", "Jane", "Has twelve fingers.", "Janeson", "https://randomuser.me/api/portraits/women/54.jpg", "Currently employed at Google.", "JaneJaneson", "JaneJ@fake.com" });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "UserId", "Bio", "FirstName", "FunFact", "LastName", "Picture", "Status", "Username", "emailAdress" },
                values: new object[] { "6228689fec05690069690022", "My name is Maureen.", "Maureen", "Can stand on my hands.", "Kelley", "https://randomuser.me/api/portraits/women/51.jpg", "Employed at IKEA.", "MaureenKelley", "MaureenK@fake.com" });

            migrationBuilder.InsertData(
                table: "Posts",
                columns: new[] { "PostId", "ParentPostId", "PostBody", "SenderId", "TargetId", "TimeStamp" },
                values: new object[] { 1, null, "Got fired today, Sadge.", "622867945b5a0b0070055011", null, new DateTime(2022, 3, 9, 10, 14, 21, 951, DateTimeKind.Local).AddTicks(2426) });

            migrationBuilder.InsertData(
                table: "Posts",
                columns: new[] { "PostId", "ParentPostId", "PostBody", "SenderId", "TargetId", "TimeStamp" },
                values: new object[] { 3, null, "Sorry about your job man.", "6228689fec05690069690022", "622867945b5a0b0070055011", new DateTime(2022, 3, 9, 10, 14, 21, 953, DateTimeKind.Local).AddTicks(6953) });

            migrationBuilder.InsertData(
                table: "Posts",
                columns: new[] { "PostId", "ParentPostId", "PostBody", "SenderId", "TargetId", "TimeStamp" },
                values: new object[] { 2, 1, "KEKW", "622868348904e60069c28aa9", null, new DateTime(2022, 3, 9, 10, 14, 21, 953, DateTimeKind.Local).AddTicks(6627) });

            migrationBuilder.CreateIndex(
                name: "IX_Posts_ParentPostId",
                table: "Posts",
                column: "ParentPostId");

            migrationBuilder.CreateIndex(
                name: "IX_Posts_SenderId",
                table: "Posts",
                column: "SenderId");

            migrationBuilder.CreateIndex(
                name: "IX_Posts_TargetId",
                table: "Posts",
                column: "TargetId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Posts");

            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}
