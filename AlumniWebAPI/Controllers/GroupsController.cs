﻿using AlumniWebAPI.Models.Domain;
using AlumniWebAPI.Models.DTO.Group;
using AlumniWebAPI.Services;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Security.Claims;
using System.Threading.Tasks;

namespace AlumniWebAPI.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class GroupsController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IGroupService _service;

        public GroupsController(IMapper mapper, IGroupService service)
        {
            _mapper = mapper;
            _service = service;
        }
        /// <summary>
        /// Fetches all groups.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult<IEnumerable<GroupReadDTO>>> GetAllGroups()
        {
            string userId = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value.Split('|')[1];
            return _mapper.Map<List<GroupReadDTO>>(await _service.GetAllGroupsAsync(userId));
        }
        /// <summary>
        /// Fetches a specified group by id.
        /// </summary>
        /// <param name="groupId"></param>
        /// <returns></returns>
        [HttpGet("{groupId}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<GroupReadDTO>> GetGroupById(int groupId)
        {
            if (!_service.GroupExists(groupId))
            {
                return NotFound();
            }
            string userId = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value.Split('|')[1];
            GroupReadDTO group = new GroupReadDTO();
            try
            {
                group = _mapper.Map<GroupReadDTO>(await _service.GetGroupByIdAsync(userId, groupId));
            }
            catch (System.Exception)
            {
                return StatusCode(StatusCodes.Status403Forbidden);
            }

            return group;
        }
        /// <summary>
        /// Creates a new group and adds the user to it.
        /// </summary>
        /// <param name="group"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> PostGroup([FromBody]GroupCreateDTO group)
        {
            Group groupDomain = _mapper.Map<Group>(group);
            string userId = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value.Split('|')[1];
            try
            {
                await _service.CreateGroupAsync(userId, groupDomain);
            }
            catch (KeyNotFoundException)
            {
                return NotFound();
            }
            GroupReadDTO newGroup = _mapper.Map<GroupReadDTO>(groupDomain);
            
            return CreatedAtAction("GetGroupById", new {groupId = newGroup.GroupId}, newGroup);
        }
        /// <summary>
        /// Attempts to add a membership to the specified group, if the body contains null the requesting user will attempt to join. If the body contains a userId the user will attempt to add
        /// the specified user to the group. If the group is private only members of that group may add new members.
        /// </summary>
        /// <param name="groupId"></param>
        /// <param name="member"></param>
        /// <returns></returns>
        [HttpPost("{groupId}/join")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> PostNewMember( int groupId, [FromBody] GroupMemberDTO member)
        {
            if (!_service.GroupExists(groupId))
            {
                return NotFound();
            }
            string userId = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value.Split('|')[1];
            if (!await _service.CanAddMember(userId, groupId))
            {
                return Forbid();
            }
            if (member.userId != null)
            {
                try
                {
                    await _service.CreateGroupMembershipAsync(member.userId, groupId);
                }
                catch (System.Exception)
                {
                    return NotFound();
                }
                
            }
            else
            {
                try
                {
                    await _service.CreateGroupMembershipAsync(userId, groupId);
                }
                catch (System.Exception)
                {
                    return NotFound();
                } 
                
            }

            return StatusCode(StatusCodes.Status201Created);
        }
    }
}
