﻿using AlumniWebAPI.Models.Domain;
using AlumniWebAPI.Models.DTO.Post;
using AlumniWebAPI.Services;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Security.Claims;
using System.Threading.Tasks;

namespace AlumniWebAPI.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class PostsController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IPostService _service;

        public PostsController(IMapper mapper, IPostService service)
        {
            _mapper = mapper;
            _service = service;
        }
        /// <summary>
        /// Fetches all posts relevant to the user.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult<IEnumerable<PostReadDTO>>> GetAllPosts()
        {
            string userId = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value.Split('|')[1];
            return _mapper.Map<List<PostReadDTO>>(await _service.GetPostsAsync(userId));
        }
        /// <summary>
        /// Fetches the Id and name of all other users the user has had direct messages to or from. 
        /// </summary>
        /// <returns></returns>
        [HttpGet("user")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult<IEnumerable<PostUserDTO>>> GetAllDirectMessages()
        {
            string userId = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value.Split('|')[1];
            List<PostReadDTO> directMessages = _mapper.Map<List<PostReadDTO>>(await _service.GetAllDirectPostsAsync(userId));
            List<PostUserDTO> conversationMembers = new List<PostUserDTO>();
            foreach (PostUserDTO post in _mapper.Map<List<PostUserDTO>>(directMessages))
            {
                if (!(post.TargetId == post.SenderId))
                {
                    if (!conversationMembers.Any(c => c.SenderId == post.SenderId && c.TargetId == post.TargetId))
                    {
                        if (!conversationMembers.Any(c => c.SenderId == post.TargetId && c.TargetId == post.SenderId))
                        {
                            conversationMembers.Add(post);
                        }
                        
                    }
                }
                
            }
            return conversationMembers;
        }
        /// <summary>
        /// Fetches all direct messages to and from the user by the specified user Id.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet("user/{userId}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<IEnumerable<PostReadDTO>>> GetAllDirectMessagesById(string userId)
        {
            if (!_service.UserExists(userId))
            {
                return NotFound("That user does not exist.");
            }
            string targetId = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value.Split('|')[1];
            return _mapper.Map<List<PostReadDTO>>(await _service.GetAllDirectPostsByIdAsync(targetId, userId));
        }
        /// <summary>
        /// Fetches all posts relevant to the specified group id.
        /// </summary>
        /// <param name="groupId"></param>
        /// <returns></returns>
        [HttpGet("group/{groupId}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<IEnumerable<PostReadDTO>>> GetAllPostsByGroupId(int groupId)
        {
            if (!_service.GroupExists(groupId))
            {
                return NotFound("That group does not exist.");
            }
            string userId = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value.Split('|')[1];
            return _mapper.Map<List<PostReadDTO>>(await _service.GetAllPostsByGroupAsync(userId, groupId));
        }
        /// <summary>
        /// Fetches all posts relevant to the specified topic.
        /// </summary>
        /// <param name="topicId"></param>
        /// <returns></returns>
        [HttpGet("topic/{topicId}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<IEnumerable<PostReadDTO>>> GetAllPostsByTopicId(int topicId)
        {
            if (!_service.TopicExists(topicId))
            {
                return NotFound("That topic does not exist.");
            }
            string userId = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value.Split('|')[1];
            return _mapper.Map<List<PostReadDTO>>(await _service.GetAllPostsByTopicIdAsync(userId, topicId));
        }
        /// <summary>
        /// Fetches the specified post and all replies to the post.
        /// </summary>
        /// <param name="postId"></param>
        /// <returns></returns>
        [HttpGet("thread/{postId}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<IEnumerable<PostReadDTO>>> GetThreadById(int postId)
        {
            if (!_service.PostExists(postId))
            {
                return NotFound("That post does not exist.");
            }
            return _mapper.Map<List<PostReadDTO>>(await _service.GetThreadByIdAsync(postId));
        }
        /// <summary>
        /// Fetches the specified post.
        /// </summary>
        /// <param name="postId"></param>
        /// <returns></returns>
        [HttpGet("{postId}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<PostReadDTO>> GetPostById(int postId)
        {
            if (!_service.PostExists(postId))
            {
                return NotFound("That post does not exist.");
            }
            return _mapper.Map<PostReadDTO>(await _service.GetPostByIdAsync(postId));
        }
        /// <summary>
        /// Creates a new post in the database.
        /// </summary>
        /// <param name="post"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        public async Task<ActionResult> PostPost([FromBody] PostCreateDTO post)
        {
            Post postDomain = _mapper.Map<Post>(post);
            string userId = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value.Split('|')[1];
            postDomain.SenderId = userId;
            postDomain.TimeStamp = DateTime.Now;
            try
            {
                await _service.CreatePost(postDomain);
            }
            catch (Exception)
            {
                return Forbid("User is not a member of the topic or group.");
            }
            PostReadDTO newPost = _mapper.Map<PostReadDTO>(postDomain);
           
            return CreatedAtAction("GetPostById", new {postId = newPost.PostId}, newPost);
        }
        /// <summary>
        /// Updates a an existing post based on the body of the request, all parameters must match the existing post except for the post body property.
        /// </summary>
        /// <param name="postId"></param>
        /// <param name="post"></param>
        /// <returns></returns>
        [HttpPut("{postId}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> UpdatePost(int postId, [FromBody] PostUpdateDTO post)
        {
            if (!_service.PostExists(postId))
            {
                return NotFound("That post does not exist.");
            }
            if (postId != post.PostId)
            {
                return BadRequest("Post id in path and post id in the body does not match.");
            }
            Post postDomain = _mapper.Map<Post>(post);
            string userId = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value.Split('|')[1];
            postDomain.SenderId = userId;
            postDomain.TimeStamp = DateTime.Now;
            try
            {
                await _service.UpdatePost(postDomain);
            }
            catch (Exception)
            {
                return Forbid("Only the body of the post can be edited.");
            }


            return NoContent();
        }
    }
}
