﻿using AlumniWebAPI.Models.Domain;
using AlumniWebAPI.Models.DTO.Topic;
using AlumniWebAPI.Services;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Security.Claims;
using System.Threading.Tasks;

namespace AlumniWebAPI.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class TopicsController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly ITopicService _service;

        public TopicsController(IMapper mapper, ITopicService service)
        {
            _mapper = mapper;
            _service = service;
        }
        /// <summary>
        /// Fetches all topics.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult<IEnumerable<TopicReadDTO>>> GetAllTopics()
        {
            return _mapper.Map<List<TopicReadDTO>>(await _service.GetAllTopicsAsync());
        }
        /// <summary>
        /// Fetches specified topic by Id.
        /// </summary>
        /// <param name="topicId"></param>
        /// <returns></returns>
        [HttpGet("{topicId}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<TopicReadDTO>> GetTopicById(int topicId)
        {
            if (!_service.TopicExists(topicId))
            {
                return NotFound("That topic does not exist.");
            }
            return _mapper.Map<TopicReadDTO>(await _service.GetTopicByIdAsync(topicId));
        }
        /// <summary>
        /// Creates a new topic based on request body. Will add the user to the newly created topic.
        /// </summary>
        /// <param name="topic"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> PostTopic([FromBody] TopicCreateDTO topic)
        {
            string userId = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value.Split('|')[1];
            Topic topicDomain = _mapper.Map<Topic>(topic);
            try
            {
                await _service.CreateTopicAsync(userId ,topicDomain);
            }
            catch (KeyNotFoundException)
            {
                return NotFound();
            }
            TopicReadDTO newTopic = _mapper.Map<TopicReadDTO>(topicDomain);
            return CreatedAtAction("GetTopicById", new {topicId = newTopic.TopicId}, newTopic);
        }
        /// <summary>
        /// Subscribes the user to the specified topic.
        /// </summary>
        /// <param name="topicId"></param>
        /// <returns></returns>
        [HttpPost("{topicId}/join")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> PostNewMember(int topicId)
        {
            if (!_service.TopicExists(topicId))
            {
                return NotFound("That topic does not exist.");
            }
            string userId = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value.Split('|')[1];
            try
            {
                await _service.AddTopicMember(userId, topicId);
            }
            catch (KeyNotFoundException)
            {

                return NotFound();
            }
            
            return StatusCode(StatusCodes.Status201Created);
        }
    }
}
