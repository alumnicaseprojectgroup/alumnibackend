﻿using AlumniWebAPI.Models.Domain;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace AlumniWebAPI.Models
{
    public class AlumniDbContext: DbContext
    {

        // Tables
        public DbSet<User> Users { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<Topic> Topics { get; set; }

        public AlumniDbContext([NotNullAttribute] DbContextOptions options) : base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Post>().HasOne(p => p.UserSender).WithMany(u => u.Posts).HasForeignKey(p => p.SenderId);

            //data seeding
            //Users
            modelBuilder.Entity<User>().HasData(new User
            {
                UserId = "622867945b5a0b0070055011",
                Username = "JohnJohnson",
                FirstName = "John",
                LastName = "Johnson",
                emailAdress = "JohnJ@fake.com",
                Status = "unemployed :(.",
                Bio = "My name is John.",
                Picture = "https://randomuser.me/api/portraits/men/61.jpg",
                FunFact = "Can flap his ears."
            });

            modelBuilder.Entity<User>().HasData(new User
            {
                UserId = "622868348904e60069c28aa9",
                Username = "JaneJaneson",
                FirstName = "Jane",
                LastName = "Janeson",
                emailAdress = "JaneJ@fake.com",
                Status = "Currently employed at Google.",
                Bio = "My name is Jane.",
                Picture = "https://randomuser.me/api/portraits/women/54.jpg",
                FunFact = "Has twelve fingers."
            });

            modelBuilder.Entity<User>().HasData(new User
            {
                UserId = "6228689fec05690069690022",
                Username = "MaureenKelley",
                FirstName = "Maureen",
                LastName = "Kelley",
                emailAdress = "MaureenK@fake.com",
                Status = "Employed at IKEA.",
                Bio = "My name is Maureen.",
                Picture = "https://randomuser.me/api/portraits/women/51.jpg",
                FunFact = "Can stand on my hands."
            });

            //Topics
            modelBuilder.Entity<Topic>().HasData(new Topic
            {
                TopicId = 1,
                Name = "Animals",
                Description = "Fun animal facts."
            });
            modelBuilder.Entity<Topic>().HasData(new Topic
            {
                TopicId = 2,
                Name = "Work",
                Description = "Anything related to work."
            });


            //Posts
            modelBuilder.Entity<Post>().HasData(new Post
            {
                PostId = 1,
                TimeStamp = System.DateTime.Now,
                SenderId = "622867945b5a0b0070055011",
                PostBody = "Got fired today, Sadge.",
                TopicId = 2
            });
            modelBuilder.Entity<Post>().HasData(new Post
            {
                PostId = 2,
                TimeStamp = System.DateTime.Now,
                SenderId = "622868348904e60069c28aa9",
                ParentPostId = 1,
                PostBody = "KEKW"
            });
            modelBuilder.Entity<Post>().HasData(new Post
            {
                PostId = 3,
                TimeStamp = System.DateTime.Now,
                SenderId = "6228689fec05690069690022",
                TargetId = "622867945b5a0b0070055011",
                PostBody = "Sorry about your job man."
            });
            modelBuilder.Entity<Post>().HasData(new Post
            {
                PostId = 4,
                TimeStamp = System.DateTime.Now,
                SenderId = "6228689fec05690069690022",
                PostBody = "Sorry about your job man."
            });

            //Groups
            modelBuilder.Entity<Group>().HasData(new Group
            {
                GroupId = 1,
                Name = "Badasses",
                Description = "For badasses only",
                isPrivate = true
            });
            
        }
    }
}
