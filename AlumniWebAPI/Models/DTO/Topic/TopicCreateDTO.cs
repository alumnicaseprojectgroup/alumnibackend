﻿namespace AlumniWebAPI.Models.DTO.Topic
{
    public class TopicCreateDTO
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
