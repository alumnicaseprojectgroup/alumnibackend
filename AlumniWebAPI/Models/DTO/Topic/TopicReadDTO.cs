﻿namespace AlumniWebAPI.Models.DTO.Topic
{
    public class TopicReadDTO
    {
        public int TopicId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
