﻿using System.Collections.Generic;

namespace AlumniWebAPI.Models.DTO.User
{
    public class UserReadDTO
    {
        public string UserId { get; set; }
        public string Username { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string emailAdress { get; set; }
        public string Picture { get; set; }
        public string Status { get; set; }
        public string Bio { get; set; }
        public string FunFact { get; set; }
        public List<int> Groups { get; set; }
        public List<int> Topics { get; set; }
    }
}
