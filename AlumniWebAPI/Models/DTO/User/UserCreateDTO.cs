﻿namespace AlumniWebAPI.Models.DTO.User
{
    public class UserCreateDTO
    {
        public string UserId { get; set; }
        public string Username { get; set; }
        public string emailAdress { get; set; }
        public string Picture { get; set; }
    }
}
