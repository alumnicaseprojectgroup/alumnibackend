﻿namespace AlumniWebAPI.Models.DTO.Group
{
    public class GroupCreateDTO
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public bool isPrivate { get; set; }
    }
}
