﻿namespace AlumniWebAPI.Models.DTO.Group
{
    public class GroupReadDTO
    {
        public int GroupId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool isPrivate { get; set; }
    }
}
