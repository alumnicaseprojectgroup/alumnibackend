﻿using System;

namespace AlumniWebAPI.Models.DTO.Post
{
    public class PostCreateDTO
    {
        public int? ParentPostId { get; set; }
        public string? TargetId { get; set; }
        public string PostBody { get; set; }
        public int? TargetGroupId { get; set; }
        public int? TopicId { get; set; }
    }
}
