﻿namespace AlumniWebAPI.Models.DTO.Post
{
    public class PostUserDTO
    {
        public string SenderId { get; set; }
        public string? TargetId { get; set; }
        public string posterName { get; set; }
        public string? TargetName { get; set; }
    }
}
