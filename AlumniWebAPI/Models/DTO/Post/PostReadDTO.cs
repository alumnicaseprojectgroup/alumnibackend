﻿using System;

namespace AlumniWebAPI.Models.DTO.Post
{
    public class PostReadDTO
    {
        public int PostId { get; set; }
        public DateTime TimeStamp { get; set; }
        public int? ParentPostId { get; set; }
        public string SenderId { get; set; }
        public string? TargetId { get; set; }
        public string PostBody { get; set; }
        public int? TargetGroupId { get; set; }
        public int? TopicId { get; set; }
        public string posterName { get; set; }
        public string? TargetName { get; set; }
        public string? TopicName { get; set; }
        public string? GroupName { get; set; }
    }
}
