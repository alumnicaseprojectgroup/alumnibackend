﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AlumniWebAPI.Models.Domain
{
    public class Group
    {
        // PK
        public int GroupId { get; set; }
        // fields
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }
        [MaxLength(200)]
        public string Description { get; set; }
        public bool isPrivate { get; set; }
        //relationships
        public ICollection<User> Members { get; set; }
    }
}
