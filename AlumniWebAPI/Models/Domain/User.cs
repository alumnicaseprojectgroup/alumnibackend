﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AlumniWebAPI.Models.Domain
{
    public class User
    {
        //PK
        [MaxLength(50)]
        public string UserId { get; set; }
        //fields
        [Required]
        [MaxLength(70)]
        public string Username { get; set; }
        [MaxLength(50)]
        public string FirstName { get; set; }
        [MaxLength(70)]
        public string LastName { get; set; }
        [MaxLength(250)]
        public string Picture { get; set; }
        [MaxLength(70)]
        public string emailAdress { get; set; }
        [MaxLength(100)]
        public string Status { get; set; }
        [MaxLength(400)]
        public string Bio { get; set; }
        [MaxLength(100)]
        public string FunFact { get; set; }
        //relationships
        public ICollection<Post> Posts { get; set; }
        public ICollection<Post> PrivateMessages { get; set; }
        public ICollection<Group> Groups { get; set; }
        public ICollection<Topic> SubscribedTopics { get; set; }
    }
}
