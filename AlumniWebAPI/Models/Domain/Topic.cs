﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AlumniWebAPI.Models.Domain
{
    public class Topic
    {
        //Pk
        public int TopicId { get; set; }
        //fields
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }
        [MaxLength(200)]
        public string Description { get; set; }
        //relationships
        public ICollection<User> Subscribers { get; set; }
    }
}
