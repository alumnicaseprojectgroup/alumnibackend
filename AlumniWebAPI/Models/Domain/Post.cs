﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AlumniWebAPI.Models.Domain
{
    public class Post
    {
        // pk
        public int PostId { get; set; }
        // fields
        [Required]
        public DateTime TimeStamp { get; set; }
        public int? ParentPostId { get; set; }
        [Required]
        [MaxLength(50)]
        public string SenderId { get; set; }
        [MaxLength(50)]
        public string? TargetId { get; set; }
        [Required]
        [MaxLength(400)]
        public string PostBody { get; set; }
        public int? TargetGroupId { get; set; }
        public int? TopicId { get; set; }
        // relationships
        public User UserSender { get; set; }
        public User Target { get; set; }
        public Post Parent { get; set; }
        public Group TargetGroup { get; set; }
        public Topic Topic { get; set; }
    }
}
