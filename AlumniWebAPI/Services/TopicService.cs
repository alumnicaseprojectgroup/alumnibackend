﻿using AlumniWebAPI.Models;
using AlumniWebAPI.Models.Domain;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AlumniWebAPI.Services
{
    public class TopicService : ITopicService
    {
        private readonly AlumniDbContext _context;

        public TopicService(AlumniDbContext context)
        {
            _context = context;
        }

        public async Task AddTopicMember(string userId, int topicId)
        {
            User user = await _context.Users.Include(u => u.SubscribedTopics).FirstOrDefaultAsync(u => u.UserId == userId);
            Topic topic = await _context.Topics.FindAsync(topicId);
            if (topic == null)
            {
                throw new KeyNotFoundException();
            }
            user.SubscribedTopics.Add(topic);
            await _context.SaveChangesAsync();
        }

        public async Task CreateTopicAsync(string userId, Topic topic)
        {
            _context.Topics.Add(topic);
            await _context.SaveChangesAsync();
            try
            {
                await AddTopicMember(userId, topic.TopicId);
            }
            catch (KeyNotFoundException)
            {
                throw new KeyNotFoundException();
            }
        }

        public async Task<IEnumerable<Topic>> GetAllTopicsAsync()
        {
            return await _context.Topics.ToListAsync();
        }

        public async Task<Topic> GetTopicByIdAsync(int topicId)
        {
            return await _context.Topics.FindAsync(topicId);
        }
        public bool TopicExists(int topicId)
        {
            return _context.Topics.Any(t => t.TopicId == topicId);
        }
    }
}

