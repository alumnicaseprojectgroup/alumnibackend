﻿using AlumniWebAPI.Models;
using AlumniWebAPI.Models.Domain;
using AlumniWebAPI.Models.DTO.User;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace AlumniWebAPI.Services
{
    public class UserService : IUserService
    {
        private readonly AlumniDbContext _context;

        public UserService(AlumniDbContext context)
        {
            _context = context;
        }

        public async Task<User> GetUserByIdAsync(string id)
        {
            return await _context.Users.Include(u => u.Groups).Include(u => u.SubscribedTopics).FirstOrDefaultAsync(u => u.UserId == id);
        }

        public bool isNull(string prop)
        {
            return prop == null ? true : false;
        }

        public async Task CreateUser(User user)
        {
            _context.Users.Add(user);
            await _context.SaveChangesAsync();
        }

        public async Task UpdatePartialUserAsync(string id, UserUpdateDTO user)
        {
            User userDomain = await _context.Users.FindAsync(id);

            if (!isNull(user.Username))
            {
                userDomain.Username = user.Username;
            }
            if (!isNull(user.FirstName))
            {
                userDomain.FirstName = user.FirstName;
            }
            if (!isNull(user.LastName))
            {
                userDomain.LastName = user.LastName;
            }
            if (!isNull(user.emailAdress))
            {
                userDomain.emailAdress = user.emailAdress;
            }
            if (!isNull(user.Status))
            {
                userDomain.Status = user.Status;
            }
            if (!isNull(user.Picture))
            {
                userDomain.Picture = user.Picture;
            }
            if (!isNull(user.FunFact))
            {
                userDomain.FunFact = user.FunFact;
            }
            if (!isNull(user.Bio))
            {
                userDomain.Bio = user.Bio;
            }
            _context.Entry(userDomain).State = EntityState.Modified;
            await _context.SaveChangesAsync();

        }

        public bool UserExists(string id)
        {
            return _context.Users.Any(e => e.UserId == id);
        }
    }
}
