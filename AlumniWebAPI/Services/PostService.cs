﻿using AlumniWebAPI.Models;
using AlumniWebAPI.Models.Domain;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AlumniWebAPI.Services
{
    public class PostService : IPostService
    {
        private readonly AlumniDbContext _context;

        public PostService(AlumniDbContext context)
        {
            _context = context;
        }

        public async Task CreatePost(Post post)
        {
            User user = await _context.Users.Include(u => u.Groups).Include(u => u.SubscribedTopics).SingleOrDefaultAsync(u => u.UserId == post.SenderId);
            List<int> topics = new List<int>();
            foreach (var topic in user.SubscribedTopics)
            {
                topics.Add(topic.TopicId);
            }
            List<int> groups = new List<int>();
            foreach (var group in user.Groups)
            {
                groups.Add(group.GroupId);
            }
            if (!topics.Contains(post.TopicId ?? default(int)) && post.TopicId != null) //if the user is not subscribed to the topic throw exception
            {
                throw new System.Exception();
            }
            if (!groups.Contains(post.TargetGroupId ?? default(int)) && post.TargetGroupId != null) //if the user is not a member to the group throw exception
            {
                throw new System.Exception();
            }
            _context.Posts.Add(post);
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<Post>> GetAllDirectPostsAsync(string userId)
        {
            return await _context.Posts.Include(p => p.UserSender).Include(p => p.Target).Where(p => p.TargetId == userId || (p.SenderId == userId && p.TargetId != null)).OrderByDescending(p => p.TimeStamp).ToListAsync();
        }

        public async Task<IEnumerable<Post>> GetAllDirectPostsByIdAsync(string userId, string targetId)
        {
            return await _context.Posts.Include(p => p.UserSender).Where(p => (p.SenderId == userId && p.TargetId == targetId) || (p.SenderId == targetId && p.TargetId == userId)).OrderByDescending(p => p.TimeStamp).ToListAsync();
        }

        public async Task<IEnumerable<Post>> GetAllPostsByGroupAsync(string userId, int groupId)
        {
            return await _context.Posts.Include(p => p.UserSender).Include(p => p.TargetGroup).Include(p => p.Topic).Where(p => p.TargetGroupId == groupId).OrderByDescending(p => p.TimeStamp).ToListAsync();
        }

        public async Task<IEnumerable<Post>> GetAllPostsByTopicIdAsync(string userId, int topicId)
        {
            return await _context.Posts.Include(p => p.UserSender).Include(p => p.TargetGroup).Include(p => p.Topic).Where(p => p.TopicId == topicId).OrderByDescending(p => p.TimeStamp).ToListAsync();
        }

        public async Task<Post> GetPostByIdAsync(int postId)
        {
            return await _context.Posts.Include(p => p.UserSender).Include(p => p.TargetGroup).Include(p => p.Topic).FirstOrDefaultAsync(p => p.PostId == postId);
        }

        public async Task<IEnumerable<Post>> GetPostsAsync(string userId)
        {
            User user = await _context.Users.Include(u => u.Groups).Include(u => u.SubscribedTopics).FirstOrDefaultAsync(u => u.UserId == userId);
            List<int> topics = new List<int>();
            foreach (var topic in user.SubscribedTopics) //get all the users topics
            {
                topics.Add(topic.TopicId);
            }
            List<int> groups = new List<int>();
            foreach (var group in user.Groups) //get all the users groups
            {
                groups.Add(group.GroupId);
            }
            return await _context.Posts.Include(p => p.UserSender)
                .Include(p => p.TargetGroup)
                .Include(p => p.Topic)
                .Where(p => p.ParentPostId == null && p.TargetId == null)
                .Where(p => (topics.Contains(p.TopicId ?? default(int)) || p.TopicId == null) && (groups.Contains(p.TargetGroupId ?? default(int))  || p.TargetGroupId == null))
                .OrderByDescending(p => p.TimeStamp)
                .ToListAsync();
        }

        public async Task<IEnumerable<Post>> GetThreadByIdAsync(int postId)
        {
            return await _context.Posts.Include(p => p.UserSender).Where(p => p.PostId == postId || p.ParentPostId == postId).OrderBy(p => p.TimeStamp).ToListAsync();
        }

        public bool PostExists(int postId)
        {
            return _context.Posts.Any(p => p.PostId == postId);
        }

        public async Task UpdatePost(Post post)
        {
            User user = await _context.Users.Include(u => u.Groups).Include(u => u.SubscribedTopics).SingleOrDefaultAsync(u => u.UserId == post.SenderId);
            Post oldPost = await _context.Posts.FindAsync(post.PostId);
            if (post.TargetGroupId != oldPost.TargetGroupId) // if any changes except the body has happened throw an exception
            {
                throw new System.Exception();
            }
            if (post.TopicId != oldPost.TopicId)
            {
                throw new System.Exception();
            }
            if (post.TargetId != oldPost.TargetId)
            {
                throw new System.Exception();
            }
            if (post.ParentPostId != oldPost.ParentPostId)
            {
                throw new System.Exception();
            }
            _context.Entry(oldPost).State = EntityState.Detached;
            _context.Entry(post).State = EntityState.Modified;
            await _context.SaveChangesAsync();

        }

        public bool UserExists(string userId)
        {
            return _context.Users.Any(u => u.UserId == userId);
        }
        public bool GroupExists(int groupId)
        {
            return _context.Groups.Any(g => g.GroupId == groupId);
        }

        public bool TopicExists(int topicId)
        {
            return _context.Topics.Any(t => t.TopicId == topicId);
        }
    }
}
