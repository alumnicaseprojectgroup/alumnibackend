﻿using AlumniWebAPI.Models.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AlumniWebAPI.Services
{
    public interface IGroupService
    {
        public Task<IEnumerable<Group>> GetAllGroupsAsync(string userId);
        public Task<Group> GetGroupByIdAsync(string userId, int groupId);
        public Task CreateGroupAsync(string userId, Group group);
        public Task CreateGroupMembershipAsync(string userId, int groupId);
        public Task<bool> CanAddMember(string userId, int groupId);
        public bool GroupExists(int groupId);
    }
}
