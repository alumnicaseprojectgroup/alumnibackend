﻿using AlumniWebAPI.Models.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AlumniWebAPI.Services
{
    public interface IPostService
    {
        public Task<IEnumerable<Post>> GetPostsAsync(string userId);
        public Task<IEnumerable<Post>> GetAllDirectPostsAsync(string userId);
        public Task<IEnumerable<Post>> GetAllDirectPostsByIdAsync(string userId, string targetId);
        public Task<IEnumerable<Post>> GetAllPostsByGroupAsync(string userId, int groupId);
        public Task<IEnumerable<Post>> GetAllPostsByTopicIdAsync(string userId, int topicId);
        public Task<IEnumerable<Post>> GetThreadByIdAsync(int postId);
        public Task<Post> GetPostByIdAsync(int postId);
        public Task CreatePost(Post post);
        public Task UpdatePost(Post post);
        public bool PostExists(int postId);
        public bool UserExists(string userId);
        public bool GroupExists(int groupId);
        public bool TopicExists(int topicId);
    }
}
