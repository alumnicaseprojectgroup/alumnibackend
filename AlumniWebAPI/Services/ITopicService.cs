﻿using AlumniWebAPI.Models.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AlumniWebAPI.Services
{
    public interface ITopicService
    {
        public Task<IEnumerable<Topic>> GetAllTopicsAsync();
        public Task<Topic> GetTopicByIdAsync(int topicId);
        public Task CreateTopicAsync(string userId, Topic topic);
        public Task AddTopicMember(string userId, int topicId);
        public bool TopicExists(int topicId);
    }
}
