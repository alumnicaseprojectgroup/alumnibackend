﻿using AlumniWebAPI.Models;
using AlumniWebAPI.Models.Domain;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AlumniWebAPI.Services
{
    public class GroupService : IGroupService
    {
        private readonly AlumniDbContext _context;

        public GroupService(AlumniDbContext context)
        {
            _context = context;
        }

        public async Task<bool> CanAddMember(string userId, int groupId)
        {
            Group group = await _context.Groups.FindAsync(groupId);
            if (!group.isPrivate)
            {
                return true;
            }
            User user = await _context.Users.Include(u => u.Groups).FirstOrDefaultAsync(u => u.UserId == userId);
            foreach (var groupIndex in user.Groups)
            {
                if (groupIndex.isPrivate && groupId == groupIndex.GroupId)
                {
                    return true;
                }
            }
            return false;
        }

        public async Task CreateGroupAsync(string userId, Group group)
        {
            _context.Groups.Add(group);
            await _context.SaveChangesAsync();
            try
            {
                await CreateGroupMembershipAsync(userId, group.GroupId);
            }
            catch (KeyNotFoundException)
            {
                throw new KeyNotFoundException();
            }
            
        }

        public async Task CreateGroupMembershipAsync(string userId, int groupId)
        {
            User user = await _context.Users.Include(u => u.Groups).FirstOrDefaultAsync(u => u.UserId == userId);

            Group group = await _context.Groups.FindAsync(groupId);
            if (group == null)
            {
                throw new KeyNotFoundException();
            }
            user.Groups.Add(group);
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<Group>> GetAllGroupsAsync(string userId)
        {
            User user = await _context.Users.Include(u => u.Groups).FirstOrDefaultAsync(x => x.UserId == userId);
            List<int> groups = new List<int>();
            foreach (var group in user.Groups)
            {
                if (group.isPrivate)
                {
                    groups.Add(group.GroupId);
                }
            }
            return await _context.Groups.Where(g => g.isPrivate != true || groups.Contains(g.GroupId)).ToListAsync();
        }

        public async Task<Group> GetGroupByIdAsync(string userId, int groupId)
        {
            Group group = await _context.Groups.FindAsync(groupId);
            if (group.isPrivate)
            {
                User user = await _context.Users.Include(u => u.Groups).FirstOrDefaultAsync(x => x.UserId == userId);
                foreach (var groupIndex in user.Groups)
                {
                    if (groupIndex.GroupId == groupId)
                    {
                        return group;
                    }
                }
                throw new System.Exception();
            }

            return group;
            
        }

        public bool GroupExists(int groupId)
        {
            return _context.Groups.Any(g => g.GroupId == groupId);
        }
    }
}
