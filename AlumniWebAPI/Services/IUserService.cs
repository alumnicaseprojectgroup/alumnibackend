﻿using AlumniWebAPI.Models.Domain;
using AlumniWebAPI.Models.DTO.User;
using System.Threading.Tasks;

namespace AlumniWebAPI.Services
{
    public interface IUserService
    {
        public Task CreateUser(User user);
        public Task<User> GetUserByIdAsync(string id);
        public Task UpdatePartialUserAsync(string id, UserUpdateDTO user);
        public bool UserExists(string id);
        public bool isNull(string prop);
    }
}
